**Godot Walk Around a Planet Tutorial**

[](https://youtu.be/zuEPWtPRsgE)

Hey guys! I’m currently working on my next game project and thought I would share this awesome solution I found! Ever wanted Mario Galaxy like mechanics in your Godot game? For my game I needed a player to be able to walk around planets. There are TONS of solutions out there for Unity, but only a handful for Godot. I was only able to find one person’s solution and it was for 2D. I hope to help by providing a simple but robust starting point for you guys in 3D!

**Instructions**

Open the “project.godot” file in the “walk_around_planet_3d” folder
The default map, “Space.tscn”, should be visible on startup. Just click play!
Use WASD to move the player around the planet!
Please let me know if you have any suggestions or find any issues!

**How it works?**

Here is the magical lines of code that makes this work:

    if transform.basis.y.normalized().cross(get_gravity_dir_vector()) != Vector3():
		look_at(planet.global_transform.origin, transform.basis.y)
    elif transform.basis.x.normalized().cross(get_gravity_dir_vector()) != Vector3():
		look_at(planet.global_transform.origin, transform.basis.x)

This can also still work with only ONE line of code! But it will be less robust:

    look_at(planet.global_transform.origin, transform.basis.x)

The heart of this solution is using the “look_at” method. Here is how the official docs explain it:
https://docs.godotengine.org/en/3.1/classes/class_spatial.html#class-spatial-method-look-at 
    void look_at ( Vector3 target, Vector3 up )
    Rotates itself so that the local -Z axis points towards the target position.

    The transform will first be rotated around the given up vector, and then fully aligned to the target by a further rotation around an axis perpendicular to both the target and up vectors.

    Operations take place in global space.

The most important thing is that the player’s local -Z axis will be pointed to the target. Usually a player's up & down axis is the Y axis. All I did was rotate the player mesh and collider around the X axis by 90 degrees so that the player’s -Z axis now appears to be the players new down axis!

Next, the look_at() method takes two parameters
Vector3 target. This is the planet’s POSITION and not the direction vector. The look_at() method will infer the direction vector from the both the player and planet position. So just use the planet’s position for the 1st parameter
Vector3 up. In order to rotate a 3D player to align to the -Z axis, you will only need to rotate twice! Which is just the X and the Y axis in this case. The look_at() method will rotate the player around this up vector FIRST. For this parameter, you can choose either the player’s local X or Y axis. Let’s choose X axis so the player will first start aligning to that -Z target position. It will rotate so that the player starts facing the planet. But we still need to rotate around the last axis. The look_at() method will then finally rotate the player around a vector (axis in this case) that is perpendicular to both the up vector (player’s local X axis) and the target direction vector (which the look_at() method will infer on its own from the specified first parameter)!

Now your player will always point their “feet” towards the planet! But there are some edge cases to think about and plan for..

**Edge Cases**

What happens if your up vector (lets say player’s local X axis again) is already “aligned” with the target position (direction) vector? If this is the case, the look_at() method cannot infer the perpendicular vector for the second axis to rotate along and you will start to see some errors in the console. 

A good way to fix this, is to check if the “up” vector is already aligned with the target position (direction) vector. If so, just make the up vector the only other player’s local vector! So if the X-axis is already aligned, just make the up vector the y-axis instead! This works because there is no way the Y axis AND the X axis can be aligned with the target position at the same time. So that’s why I added this extra check to our original one line of code: 

    if transform.basis.y.normalized().cross(get_gravity_dir_vector()) != Vector3():
		look_at(planet.global_transform.origin, transform.basis.y)
	elif transform.basis.x.normalized().cross(get_gravity_dir_vector()) != Vector3():
		look_at(planet.global_transform.origin, transform.basis.x)


**Player Movement** 

This is the easy part. Just move the player along its local X and Y axis! Remember that we are using Y now instead of Z because we had to reorient the player to accommodate the specific axis the look_at() method needs to align to. 

I used a kinematic body for the player in this demo but this solution will also work for rigidbody players that are in “character” mode!

**Final**

With this solution, the player will always be “tidally” locked to the planet even if a player goes over a slope or rough terrain on the planet. If you wanted a more true Mario Galaxy like experience, then instead of setting the Vector3 target to the planets center, try making the target a point on the surface of the planet. That way your player will be able to hug the surface of the planet shape instead! 

I will be implementing gravity for the player so so please stay tuned for that!

Please let me know how this solution works for you and if you had to change this solution to fix any issues. I am happy to help if you run into any issues!

(Yout may notice the player will start to gain altitude as they move. This is expected and will be fixed once gravity system is added!)

